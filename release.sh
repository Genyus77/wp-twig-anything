#!/usr/bin/env bash

# USAGE
# 1. Create a release tag in git:
#    git tag -a 1.6.5 -m 'Twig Anything 1.6.5'
# 2. Run this script and pass in the version number :
#    ./release.sh 1.6.5

# Remove an existing ZIP with the same release name
rm -f twig-anything-$1.zip

# Create a new ZIP archive
git archive -v -9 \
    --output="twig-anything-$1.zip" \
    --prefix="twig-anything/" \
    --worktree-attributes $1:plugin_src

scp twig-anything-$1.zip twiganything@twiganything.com:/home/twiganything/downloads/

scp twig-anything-$1.zip twiganything@twiganything.com:/home/twiganything/web2/wp-content/plugins/twig-anything.zip
ssh twiganything@twiganything.com 'cd /home/twiganything/web2/wp-content/plugins/ && rm -rf twig-anything && unzip -o twig-anything.zip && rm twig-anything.zip'

scp twig-anything-$1.zip kozovodstvo@kozovod.com:/home/kozovodstvo/web/wp-content/plugins/twig-anything.zip
ssh kozovodstvo@kozovod.com 'cd /home/kozovodstvo/web/wp-content/plugins/ && rm -rf twig-anything && unzip -o twig-anything.zip && rm twig-anything.zip'