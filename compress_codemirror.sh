#!/usr/bin/env bash

# Script to combine and compress CodeMirror resources.
#
# Run it after updating CodeMirror version,
# then update versions in TwigAnything::onAdminEnqueueScripts()

uglifyjs --timings --wrap TwigAnythingCodeMirrorWrapper --compress \
    --output=plugin_src/js/codemirror-compressed.js \
    node_modules/codemirror/lib/codemirror.js \
    js/codemirror-wrapper-addons-prefix.js \
    node_modules/codemirror/addon/mode/multiplex.js \
    node_modules/codemirror/addon/display/fullscreen.js \
    node_modules/codemirror/addon/display/placeholder.js \
    node_modules/codemirror/mode/css/css.js \
    node_modules/codemirror/mode/javascript/javascript.js \
    node_modules/codemirror/mode/xml/xml.js \
    node_modules/codemirror/mode/htmlmixed/htmlmixed.js \
    node_modules/codemirror/mode/twig/twig.js \
    js/codemirror-wrapper-suffix.js

uglifycss --output plugin_src/css/codemirror.css \
    node_modules/codemirror/lib/codemirror.css